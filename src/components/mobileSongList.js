import React from 'react'

const MobileSongList = ({ searchData=[], status=false }) => {
  return (
    <>
        {status && <div className="text-white font-bold text-sm py-5 text-center">No music found</div>}
        {status === false && searchData.map(data => {
            return (
                <ListCard key={data.id} data={data}/>
            )
        })}
    </>
  )
}

export const ListCard = ({ data }) => {
    const {song_name, band_name, year, downloaded} = data

    return (
        <div className="mt-10">
            <div className="flex p-2 max-w-md md:max-w-xl justify-between bg-[#2e2e32] rounded-3xl">
                <div className="md:text-sm text-xs font-semibold">
                    <div className="flex-1 pl-2 lg:text-lg sm:text-sm">
                        <div className="text-white font-semibold">{song_name}</div>
                        <div className="text-white font-thin">{band_name}</div>
                        <div className="text-white text-sm font-thin">{year}</div>
                    </div>
                </div>
                <div className={`${downloaded ? "bg-[#32E6B7] text-black" : "border-2 border-[#D9506B] text-[#D9506B]"} px-2 py-1 rounded-xl mx-3 my-5 text-black md:text-sm text-xs font-semibold`}>
                    {downloaded ? "Downloaded" : "Not Downloaded"}
                </div>
            </div>
        </div>
    )
}

export default MobileSongList