import React, { useState } from 'react'

import control from '../assets/sidebarIcons/control.png'
import logo from '../assets/Logo BS.png'

import musicList from '../assets/sidebarIcons/my-music-list.png'
import lyrics from '../assets/sidebarIcons/my-lyrics-collection.png'
import chat from '../assets/sidebarIcons/chat-with-musicians.png'
import logOut from '../assets/sidebarIcons/logout.png'

const Sidebar = ({ send }) => {
    const [open, setOpen] = useState(true)

    const menus = [
        {title: "My music list", src: musicList, bold: true},
        {title: "My lyrics collection", src: lyrics},
        {title: "Chat with musicians", src: chat},
    ]

    return (
        <div className={`fixed z-40 ${open ? 'lg:w-[19%] sm:w-[80%]' : 'lg:w-[6%] sm:w-0'} duration-300 min-h-screen bg-gradient-to-b from-[#900f11] to-[#FF8786] pl-5 pt-8`}>
            {<div className="inset-y-0 left-0 w-1/20">
                <img src={logo} alt="logo" className={`cursor-pointer ${!open && "h-12"}`} />
            </div>}
            <div className="pt-20">
                {/* <h3 className="text-white origin-left font-semibold">My music list</h3>  */}
                <ul>
                    {menus.map((menu, index) => (
                        <li key={index} className="text-white lg:my-7 flex items-center gap-x-4 cursor-pointer p-2 hover:bg-light-white rounded-md">
                            <img src={menu.src} alt={menu.title} />
                            <span className={`${!open && "hidden"} ${menu.bold && "font-bold"} origin-left`}>{menu.title}</span>
                        </li>
                    ))}
                    <li key="Logout" className="text-white lg:my-7 flex items-center gap-x-4 cursor-pointer p-2 hover:bg-light-white rounded-md"
                         onClick={() => send({type: 'Log out'})}
                    >
                        <img src={logOut} alt="Log out" />
                        <span 
                            className={`${!open && "hidden"} origin-left`}>Logout</span>
                    </li>
                </ul>
            </div>
            <div className="min-h-full w-full flex flex-row justify-end items-center content-center">
                <img 
                    src={control} 
                    className="cursor-pointer" 
                    onClick={() => setOpen(!open)}    
                />
            </div>
        </div>
    )
}

export default Sidebar