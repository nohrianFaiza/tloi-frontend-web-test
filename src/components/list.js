import React from 'react'

import tricircle from '../assets/tricircle.png'

const List = ({ searchData=[], status=false }) => {
    return (
        <>
            {status && <div className="text-white font-bold text-center">No music found</div>}
            {status === false && searchData.map(data => {
                return (
                    <ListCard key={data.id} data={data}/>
                )
            }) }
        </>
    )
}

export const ListCard = ({ data }) => {
    const {song_name, band_name, year, downloaded} = data

    // border right width: 10px (border-r-8)
                // top: 2 rem (top-8)
                // left: -1.6rem (-left-6)
                // bottom: 3rem (bottom-12)
    return (
        <div className="relative flex flex-col ml-20">
            <div className="bg-[#3f3f44] w-full lg:px-4 sm:px-10 px-6 py-4 mb-8 rounded-3xl border-r-7 -ml-6 pl-6 flex items-center justify-between my-4">
                <div className="lg:text-lg md:text-sm sm:text-xs font-semibold">
                    <div className="flex-1 pl-2 lg:text-lg sm:text-sm">
                        <div className="text-white font-semibold">{song_name}</div>
                        <div className="text-white font-thin">{band_name}</div>
                        <div className="text-white text-sm font-thin">{year}</div>
                    </div>
                </div>
                <div
                    className={`${downloaded ? "bg-[#32E6B7] text-black" : "border-2 border-[#D9506B] text-[#D9506B]"} font-bold lg:py-3 rounded-xl mx-6 px-4`}
                >
                    {downloaded ? "Already downloaded" : "Not Yet, Download Now !"}
                </div>
                <div className="text-white text-3xl flex flex-row-reverse items-center lg:pr-3 justify-center">
                    <img alt="three circle" src={tricircle}/>
                </div>
            </div>
            <div className="absolute transform -translate-x-20 bg-[#2e2e32] w-full rounded-3xl lg:px-4 sm:px-10 px-6 py-4 mb-8  border-r-8 border-[#2e2e32] -ml-6 pl-6 flex items-center justify-between my-4">
                <div className="lg:text-lg md:text-sm sm:text-xs font-semibold">
                <div className="flex-1 pl-2 lg:text-lg sm:text-sm">
                    <div className="text-white font-semibold">{song_name}</div>
                    <div className="text-white font-thin">{band_name}</div>
                    <div className="text-white text-sm font-thin">{year}</div>
                </div>
                </div>
                <div  className={`${downloaded ? "bg-[#32E6B7] text-black" : "border-2 border-[#D9506B] text-[#D9506B]"} font-bold lg:py-3 rounded-xl mx-6 px-4`}>
                    {downloaded ? "Already downloaded" : "Not Yet, Download Now !"}
                </div>
            </div>
            <div className={`absolute border-r-8 ${downloaded ? "border-[#32E6B7]" : "border-[#D9506B]"} top-8 -left-[105px] bottom-12 dark:border-gray-800`}></div>
        </div>

    ) 
}

export default List