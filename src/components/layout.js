import React from 'react'

import logo from '../assets/Logo BS.png'
import hero from '../assets/music-hero 1.png'

const Layout = () => {
  return (
    <>
        <div className="absolute bg-[#C50000] w-4/9 left-0 top-0 min-h-screen px-20">
            <div class="absolute inset-y-0 left-0 h-full w-1/10 p-10">
                <img alt="icon" src={logo} class="cursor-pointer" />
            </div>
            <div className="flex h-screen">
                <img className="m-auto" src={hero} alt="hero" />
            </div>
        </div>
    </>
  )
//   py-[7%] px-[5%]
}

export default Layout