import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'

import google from '../assets/buttonIcons/Gmail.png'
import windows from '../assets/buttonIcons/Windows.png'
import yahoo from '../assets/buttonIcons/Yahoo.png'
import apple from '../assets/buttonIcons/Apple.png'
import facebook from '../assets/buttonIcons/Facebook.png'

const LoginSchema = Yup.object().shape({
    password: Yup.string()
        .min(8, 'Password is too short!')
        .max(25, 'Password is too long!')
        .required('Password is required'),

})

const providers = [
    {title: "Google", src: google, color: "bg-[#FFFFFF]", border: "border border-[#ECECEC]"},
    {title: "Windows", src: windows, color: "bg-[#303030]"},
    {title: "Yahoo", src: yahoo, color: "bg-[#9B11B1]"},
    {title: "Apple", src: apple, color: "bg-[#000000]"},
    {title: "Facebook", src: facebook, color: "bg-[#3580F0]"},
] 

export const LoginCard = ({ send }) => {
  return (
    <>
        <div className="w-full min-h-screen flex flex-col sm:grid sm:place-items-center sm:h-screen sm:justify-center items-center">
            <div className="card bg-white rounded-xl w-full sm:max-w-md px-5 py-8 mx-auto shadow-md shadow-red-300">
                <p className="mb-2">Hello, 👋</p>
                <h2 className="text-2xl font-bold mb-2">Welcome to the black parade</h2>
                <p className="mb-2">Please enter your details here</p>
                <Formik
                    initialValues={{ emailOrUsername: '', username: '', email: '', password: '' }}
                    validationSchema={LoginSchema}
                    validate={(values) => {
                        const errors = {}

                        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values.emailOrUsername)) {
                            // if it's not an email, then it must be a username
                            if (values.emailOrUsername.length < 6) {
                                errors.emailOrUsername = 'Username is too short!'
                            } else if (values.emailOrUsername.length > 20) {
                                errors.emailOrUsername = 'Username is too long!'
                            } else if(values.emailOrUsername.includes('@')) {
                                errors.emailOrUsername = 'Invalid username'
                            }

                            values.username = values.emailOrUsername

                            // clear previous inputted email
                            values.email = ''
                        } else { // the value is an email
                            const characters = values.emailOrUsername.substring(0, values.emailOrUsername.indexOf('@'))
                            
                            if (characters.length < 5 ) {
                                errors.emailOrUsername = 'Email is too short!'
                            } else if (characters.length > 70) {
                                errors.emailOrUsername = 'Email is too long!'
                            }
                            
                            values.email = values.emailOrUsername

                            // clear previous inputted username
                            values.username = ''
                        }

                        return errors
                    }}
                    onSubmit={(values) => {
                        setTimeout(() => {
                            send({type: 'Submit login', user: values})
                        }, 400);
                    }}
                >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                }) => (
                <Form
                    onSubmit={handleSubmit}
                    className="space-4"
                >
                <div className="mb-4">
                    <label className="block mb-1 font-semibold" htmlFor="emailOrUsername">
                        Email or Username
                    </label>
                    <input
                        className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                        type="text"
                        name="emailOrUsername"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.emailOrUsername}
                        placeholder="eg: homersimpsons@gmail.com / homerSimps"
                    />
                    {errors.emailOrUsername && touched.emailOrUsername && errors.emailOrUsername}
                </div>
                <div className="mb-4">
                    <label className="block mb-1 font-semibold" htmlFor="password"> 
                        Password
                    </label>
                    <input
                        className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                        type="password"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        placeholder="Minimum 8 characters"
                    />
                    {errors.password && touched.password && errors.password}
                </div>
                <div className="mt-6 flex items-center justify-end">
                    <a href="#" className="text-sm underline text-[#0066FF]"> Forget password? </a>
                </div>
                <div className="mt-6">
                    <button 
                        className="w-full inline-flex items-center justify-center px-4 py-2 bg-gradient-to-b from-[#FD6766] to-[#BD2E2D] border border-transparent rounded-xl font-semibold capitalize text-white hover:bg-red-700 active:bg-red-700 focus:outline-none focus:border-red-700 focus:ring focus:ring-red-200 disabled:opacity-25 transition"
                        type="submit" disabled={isSubmitting}
                    >
                        Log In
                    </button>
                </div>
                </Form>
            )}
                </Formik>
                <div className="relative flex pt-4 items-center">
                    <div className="flex-grow border-t border-[#F0F0F0]"></div>
                    <span className="flex-shrink mx-4 text-sm text-[#757575]">Or</span>
                    <div className="flex-grow border-t border-[#F0F0F0]"></div>
                </div>
                <p className="text-center text-sm font-bold pb-4">Login using</p>
                <div className="flex flex-row items-center justify-center">
                    {providers.map(provider => (
                        <div key={provider.title}>
                            <button className={`flex flex-col w-15 h-15 mx-2 px-4 py-4 rounded-lg ${provider.color} ${provider.border ? provider.border : ""}cursor-pointer`}>
                                <img className="object-fill" src={provider.src} />
                            </button>
                        </div>
                    ))}
                </div>
                <div className="mt-6 text-center">
                    <p>
                        Not registered? <a onClick={() => send({type: 'Go to signup'})} className="underline text-[#0066FF] font-bold">Create an account</a>
                    </p>
                </div>
            </div>
        </div>
    </>
  )
}

export const MobileLoginCard = ({ send }) => {
    return (
        <div class="card bg-white rounded-xl px-5 py-8">
            <p className="mb-2">Hello, 👋</p>
            <h2 className="text-xl font-bold mb-2">Welcome to the black parade</h2>
            <p className="mb-2">Please enter your details here</p>
            <Formik
                initialValues={{ emailOrUsername: '', username: '', email: '', password: '' }}
                validationSchema={LoginSchema}
                validate={(values) => {
                    const errors = {}

                    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values.emailOrUsername)) {
                        // if it's not an email, then it must be a username
                        if (values.emailOrUsername.length < 6) {
                            errors.emailOrUsername = 'Username is too short!'
                        } else if (values.emailOrUsername.length > 20) {
                            errors.emailOrUsername = 'Username is too long!'
                        } else if(values.emailOrUsername.includes('@')) {
                            errors.emailOrUsername = 'Invalid username'
                        }

                        values.username = values.emailOrUsername

                        // clear previous inputted email
                        values.email = ''
                    } else { // the value is an email
                        const characters = values.emailOrUsername.substring(0, values.emailOrUsername.indexOf('@'))
                        
                        if (characters.length < 5 ) {
                            errors.emailOrUsername = 'Email is too short!'
                        } else if (characters.length > 70) {
                            errors.emailOrUsername = 'Email is too long!'
                        }
                        
                        values.email = values.emailOrUsername

                        // clear previous inputted username
                        values.username = ''
                    }

                    return errors
                }}
                onSubmit={(values) => {
                    setTimeout(() => {
                        send({type: 'Submit login', user: values})
                    }, 400);
                }}
            >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            /* and other goodies */
            }) => (
            <Form
                onSubmit={handleSubmit}
                className="space-4"
            >
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="emailOrUsername">
                    Email or Username
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="text"
                    name="emailOrUsername"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.emailOrUsername}
                    placeholder="eg: homersimpsons@gmail.com / homerSimps"
                />
                {errors.emailOrUsername && touched.emailOrUsername && errors.emailOrUsername}
            </div>
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="password"> 
                    Password
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="password"
                    name="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    placeholder="Minimum 8 characters"
                />
                {errors.password && touched.password && errors.password}
            </div>
            <div className="mt-6 flex items-center justify-end">
                <a href="#" className="text-sm underline text-[#0066FF]"> Forget password? </a>
            </div>
            <div className="mt-6">
                <button 
                    className="w-full inline-flex items-center justify-center px-4 py-2 bg-gradient-to-b from-[#FD6766] to-[#BD2E2D] border border-transparent rounded-xl font-semibold capitalize text-white hover:bg-red-700 active:bg-red-700 focus:outline-none focus:border-red-700 focus:ring focus:ring-red-200 disabled:opacity-25 transition"
                    type="submit" disabled={isSubmitting}
                >
                    Log In
                </button>
            </div>
            </Form>
        )}
            </Formik>

            <div className="relative flex pt-4 items-center">
                <div className="flex-grow border-t border-[#F0F0F0]"></div>
                <span className="flex-shrink mx-4 text-sm text-[#757575]">Or</span>
                <div className="flex-grow border-t border-[#F0F0F0]"></div>
            </div>
            <p className="text-center text-sm font-bold pb-4">Login using</p>
            <div className="flex flex-row items-center justify-center">
                {providers.map(provider => (
                    <div key={provider.title}>
                        <button className={`flex flex-col w-15 h-15 mx-2 px-4 py-4 rounded-lg ${provider.color} ${provider.border ? provider.border : ""}cursor-pointer`}>
                            <img className="object-fill" src={provider.src} />
                        </button>
                    </div>
                ))}
            </div>
            <div className="mt-6 text-center">
                <p>
                    Not registered? <a onClick={() => send({type: 'Go to signup'})} href="#" className="underline text-[#0066FF] font-bold">Create an account</a>
                </p>
            </div>
        </div>
    )
}