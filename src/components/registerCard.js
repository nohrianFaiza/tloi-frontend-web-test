import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import YupPassword from 'yup-password'

// extend yup
YupPassword(Yup)

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/ 
const SignupSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email!')
        .min(5, 'Email is too short!')
        .max(70, 'Email is too long'),
    username: Yup.string()
        .min(6, 'Username is too short!')
        .max(18, 'Username is too long!'),
    name: Yup.string()
        .min(2, 'Name is too short')
        .max(50, 'Name is too long')
        .required('Name is required'),
    mobileNumber: Yup.string()
        .matches(phoneRegExp, 'Invalid mobile number')
        .min(8, 'Mobile number too few')
        .max(14, 'Mobile number too long')
        .required('Mobile number is required'),
    password: Yup.string()
        .password()
        .minNumbers(1, "Password must at least contain one number")
        .minUppercase(1, "Password must at least contain one uppercase character")
        .min(8, "Password too short!")
        .max(25, "Password can't be longer than 25 characters")
        .required('Password is required'),
    confirmedPassword: Yup.string()
        .password()
        .minNumbers(1)
        .minUppercase(1)
        .min(8)
        .max(25)
        .required('You need to confirm the password'),
})

export const RegisterCard = ({ send}) => {
    return (
        <>
            <div className="w-full py-6 max-h-screen flex flex-col sm:justify-center items-center">
                <div className="card bg-white max-h-[75%] rounded-xl w-full sm:max-w-md px-5 py-8 mx-auto shadow-md shadow-red-300  overflow-y-scroll scrollbar-hide">
                    <h2 className="text-2xl font-bold mb-2">Create new account?</h2>
                    <p className="mb-2 text-[#9B9B9B]">Create new account to experience to the awesomeness music</p>
                    <Formik
                        initialValues={{ 
                            email: '', 
                            username: '',
                            mobileNumber: '',
                            password: '',
                            confirmedPassword: ''
                        }}
                        validationSchema={SignupSchema}
                        validate={(values) => {
                            const errors = {}

                            if (values.username.includes('@')) { // username can't have the same value as email
                                errors.username = 'Invalid username'
                            }

                            if (!values.username && !values.email) {
                                errors.username = 'You must fill either or both username and email'
                                errors.email = 'You must fill either or both username and email'
                            }

                            // validate password (both of the passwords must match)
                            if (values.confirmedPassword !== values.password) {
                                errors.confirmedPassword = "Passwords didn't match!"
                            }

                            return errors
                        }}
                        onSubmit={(values) => {
                            console.log(values)
                            setTimeout(() => {
                                send({ type: 'Submit signup', user: values })
                            }, 400);
                        }}
                    >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                        /* and other goodies */
                    }) => (
                    <Form
                        onSubmit={handleSubmit}
                        className="space-4"
                    >
                    <div className="mb-4">
                        <label className="block mb-1 font-semibold" htmlFor="username">
                           Username
                        </label>
                        <input
                            className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                            type="text"
                            name="username"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.username}
                            placeholder="e.g: TomJohn"
                        />
                        {errors.username && touched.username && errors.username}
                    </div>
                    <div className="mb-4">
                        <label className="block mb-1 font-semibold" htmlFor="email"> 
                            Email
                        </label>
                        <input
                            className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                            type="email"
                            name="email"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.email}
                            placeholder="e.g: example@mail.com"
                        />
                        {errors.email && touched.email && errors.email}
                    </div>
                    <div className="mb-4">
                        <label className="block mb-1 font-semibold" htmlFor="name"> 
                            Name
                        </label>
                        <input
                            className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                            type="text"
                            name="name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.name}
                            placeholder="e.g: Tom John"
                        />
                        {errors.name && touched.name && errors.name}
                    </div>
                    <div className="mb-4">
                        <label className="block mb-1 font-semibold" htmlFor="mobileNumber"> 
                            Mobile Number
                        </label>
                        <input
                            className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                            type="text"
                            name="mobileNumber"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.mobileNumber}
                            placeholder="e.g: 08123456789"
                        />
                        {errors.mobileNumber && touched.mobileNumber && errors.mobileNumber}
                    </div>
                    <div className="mb-4">
                        <label className="block mb-1 font-semibold" htmlFor="password"> 
                            Password
                        </label>
                        <input
                            className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                            type="password"
                            name="password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                            placeholder="Minimum 8 characters"
                        />
                        {errors.password && touched.password && errors.password}
                    </div>
                    <div className="mb-4">
                        <label className="block mb-1 font-semibold" htmlFor="confirmedPassword"> 
                            Re-enter Password
                        </label>
                        <input
                            className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                            type="password"
                            name="confirmedPassword"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.confirmedPassword}
                            placeholder="Minimum 8 characters"
                        />
                        {errors.confirmedPassword && touched.confirmedPassword && errors.confirmedPassword}
                    </div>

                    <div className="mt-6 py-4">
                        <button 
                            className="w-full inline-flex items-center justify-center px-4 py-2 bg-gradient-to-b from-[#FD6766] to-[#BD2E2D] border border-transparent rounded-xl font-semibold capitalize text-white hover:bg-red-700 active:bg-red-700 focus:outline-none focus:border-red-700 focus:ring focus:ring-red-200 disabled:opacity-25 transition"
                            type="submit" disabled={isSubmitting}
                        >
                            Sign up
                        </button>
                        <button 
                            onClick={() => send({ type: 'Go to login' })}
                            className="mt-4 w-full flex-col items-center justify-center px-4 py-2 bg-[#F4F4F4] disabled:opacity-25 transition"
                            disabled={isSubmitting}
                        >
                            <p>Oh, I have an account</p>
                            <p className="font-bold text-[#BD2E2D]">Login now</p>
                        </button>
                    </div>
                    </Form>
                )}
                    </Formik>
                </div>
        </div>
        </>
    )
}

export const MobileRegisterCard = ({ send }) => {
    return (
        <div class="card bg-white rounded-xl px-5 py-8">
            <h2 className="text-2xl font-bold mb-2">Create new account?</h2>
            <p className="mb-2 text-[#9B9B9B]">Create new account to experience to the awesomeness music</p>
            <Formik
                initialValues={{ 
                    email: '', 
                    username: '',
                    mobileNumber: '',
                    password: '',
                    confirmedPassword: ''
                }}
                validationSchema={SignupSchema}
                validate={(values) => {
                    const errors = {}

                    if (values.username.includes('@')) { // username can't have the same value as email
                        errors.username = 'Invalid username'
                    }

                    if (!values.username && !values.email) {
                        errors.username = 'You must fill either or both username and email'
                        errors.email = 'You must fill either or both username and email'
                    }

                    // validate password (both of the passwords must match)
                    if (values.confirmedPassword !== values.password) {
                        errors.confirmedPassword = "Passwords didn't match!"
                    }

                    return errors
                }}
                onSubmit={(values) => {
                    setTimeout(() => {
                        send({type: 'Submit signup', user: values})
                    }, 400);
                }}
            >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
            }) => (
            <Form
                onSubmit={handleSubmit}
                className="space-4"
            >
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="username">
                    Username
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="text"
                    name="username"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.username}
                    placeholder="e.g: TomJohn"
                />
                {errors.username && touched.username && errors.username}
            </div>
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="email"> 
                    Email
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="email"
                    name="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    placeholder="e.g: example@mail.com"
                />
                {errors.email && touched.email && errors.email}
            </div>
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="name"> 
                    Name
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="text"
                    name="name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                    placeholder="e.g: Tom John"
                />
                {errors.name && touched.name && errors.name}
            </div>
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="mobileNumber"> 
                    Mobile Number
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="text"
                    name="mobileNumber"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.mobileNumber}
                    placeholder="e.g: 08123456789"
                />
                {errors.mobileNumber && touched.mobileNumber && errors.mobileNumber}
            </div>
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="password"> 
                    Password
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="password"
                    name="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    placeholder="Minimum 8 characters"
                />
                {errors.password && touched.password && errors.password}
            </div>
            <div className="mb-4">
                <label className="block mb-1 font-semibold" htmlFor="confirmedPassword"> 
                    Re-enter Password
                </label>
                <input
                    className="py-2 px-3 bg-[#F8F8F8] border border-gray-300 focus:border-red-300 focus:outline-none focus:ring focus:ring-red-200 focus:ring-opacity-50 rounded-md shadow-sm disabled:bg-gray-100 mt-1 block w-full" 
                    type="password"
                    name="confirmedPassword"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.confirmedPassword}
                    placeholder="Minimum 8 characters"
                />
                {errors.confirmedPassword && touched.confirmedPassword && errors.confirmedPassword}
            </div>

            <div className="mt-6 py-4">
                <button 
                    className="w-full inline-flex items-center justify-center px-4 py-2 bg-gradient-to-b from-[#FD6766] to-[#BD2E2D] border border-transparent rounded-xl font-semibold capitalize text-white hover:bg-red-700 active:bg-red-700 focus:outline-none focus:border-red-700 focus:ring focus:ring-red-200 disabled:opacity-25 transition"
                    type="submit" disabled={isSubmitting}
                >
                    Sign up
                </button>
                <button 
                    onClick={() => send({ type: 'Go to login' })}
                    className="mt-4 w-full flex-col items-center justify-center px-4 py-2 bg-[#F4F4F4] disabled:opacity-25 transition"
                    disabled={isSubmitting}
                >
                    <p>Oh, I have an account</p>
                    <p className="font-bold text-[#BD2E2D]">Login now</p>
                </button>
            </div>
            </Form>
                )}
            </Formik>
        </div>
    )
}