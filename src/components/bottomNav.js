import React from 'react'

import musicList from '../assets/sidebarIcons/my-music-list.png'
import lyrics from '../assets/sidebarIcons/my-lyrics-collection.png'
import chat from '../assets/sidebarIcons/chat-with-musicians.png'
import logOut from '../assets/sidebarIcons/logout.png'

const BottomNav = ({ send }) => {
  const menus = [
    {title: "My music", src: musicList},
    {title: "My lyrics", src: lyrics},
    {title: "Chat", src: chat},
  ]

  return (
    <section className="block fixed inset-x-0 bottom-0 z-10 bg-gradient-to-r from-[#900f11] to-[#FF8786]">
        <div className="flex justify-between">
            {menus.map((menu, index) => (
                <a key={index} href="#" className="w-full hover:text-bold justify-center inline-block text-center pt-2 pb-1 text-2xs">
                    {/* Icon here */}
                    <div className="flex flex-col justify-center items-center" >
                        <img src={menu.src} alt={menu.title} />
                    </div>
                    <span classname="block">{menu.title}</span>
                  </a>
            ))}
            <a
              onClick={() => send({type: 'Log out'})} 
              key="Logout" href="#" className="w-full hover:text-bold justify-center inline-block text-center pt-2 pb-1 text-2xs">
                {/* Icon here */}
                <div className="flex flex-col justify-center items-center" >
                    <img src={logOut} alt="Logout" />
                </div>
                <span classname="block">Logout</span>
            </a>
        </div>
    </section>
  )
}

export default BottomNav