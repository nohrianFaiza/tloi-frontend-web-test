import React from 'react'

import logo from '../assets/Logo BS.png'

const MobileLayout = ({ children }) => {
  return (
    <body className="relative">
        <div className="container mx-auto p-8 bg-[#C50000] min-h-screen min-w-full">
            {/* container for logo and card */}
            <div className="mx-auto max-w-md">
                {/* Logo container centered */}
                <div className="py-5 pt-0 flex flex-col justify-center items-center"> 
                    <img className="w-17" alt="logo icon" src={logo} />
                </div>
                {/* Form container */}
                {children}
            </div>
        </div>
    </body>
  )
}

export default MobileLayout