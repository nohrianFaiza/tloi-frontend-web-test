import React from 'react'
import { useMachine } from '@xstate/react';
import toast, { Toaster } from 'react-hot-toast';

import { bsMachine } from './machines/bsMachine'

import LoginPage from './pages/Login'
import DashboardPage from './pages/Dashboard'
import RegisterPage from './pages/Register';

function App() {
  const [state, send] = useMachine(bsMachine)

  console.log('state', state)

  if (state.matches('User not found')) {
    toast.error('User not found, please sign up first')
  }
  
  return (
    <>
      <Toaster />
      {state.matches('Login Page') && (
        <LoginPage 
          send={send}
        />
      )}
      {state.matches('Dashboard Page') && (
        <DashboardPage send={send} />
      )}
      {state.matches('Signup Page') && (
        <RegisterPage send={send} />
      )}
    </>
  );
}

export default App;
