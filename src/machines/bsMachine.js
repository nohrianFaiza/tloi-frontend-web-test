import { assign, createMachine } from 'xstate'

export const bsMachine = 
/** @xstate-layout N4IgpgJg5mDOIC5QCEA2BDAxgawAQAV0AndCMXAWSwAsBLAOzADoAZAeygYPRgGIBlAK4AjALa0ALrlQcGiUAAc2sSbTb15IAB6IAtAEYALEwDMJ-QHYLABgBsAVjMAmJ2YsAaEAE89Fk-aYADgtA6wBOfWDDa0MQgF84zzQsPEISMkoaBmZ2TnpuPgBxNlwJEpUoekEFTSUVCTUNJG09fQDQ63snezsQowtbTx8EXVs7JnsLQxMIwx7bfScEpIwcbnTyKkw6RiYAEXRYamE2YggCsF5c3DZBCVrlVXVNHRHI-SZDQJNAxf0wky2bpAoZ6JwAphAtqGWxWQJOQKBWzLEDJNZpUibLK7fi0SrVC4CETiKQVKo1Zp1J5NUCvH7WT6GJxMiLmfS9UEjeyOJhhCz2ML2YImJncwwotGpYiYzLbbJMXH4hSE4qlEoyPIPeqNF6+ExMTrhFz6WxhYKRJyc3SGOYGlx+EJhG2m8WJVGrKUbWU7ZgAYWoYBwDCguAAZmwiLhBLAwEReBB1MwGAA3NjYZiS9YyrY+pj+wPYYNhiNRmNEBAptiYdANdQAbWsAF0tdTdQgek5IUiAZ0LE5YQCTFapvrAbZAvYgQMbK6Viksxkc-KAKpl3D0NhScOCegQXhaWASGvMdChiSxgAU3Ws1gAlLxMxjF9jmKvY+vN8WdxAW7WaS0Rj5Jh+WiQwwj5ewmSsMIrRMCwPknPlwNhexIhFewEjdDcyHgZpH2lZ85V2XIuEIGBfx1ZpXgMeDeTNf4Zi6AFXECYdDA+YJQidQIwOscwwglD0FyxIjmAOI4TjOC4KOeKjEDCaxOzMJi2j4yJHCtU0LFMWxxwFRjbB+CxBPnJ8RNzRVyWkylHj-NtXH1VknW5cJ+R+TTFgmEcZzcQIARM9ECPM+V8yDegQ3DSNo1jGT-1eE0wgmSDJx4p0bUtbxfBhT59DgkxXPA-KTACz1sxfJg30jDct1uXdYrbBKJlCcFrD8tp-B42CrAmLT+W+XKb0CErhO9bJ6rkkYnE6T5vl+JwGKBScMuGXRfkCHK4OdOwIgcTC4iAA */
createMachine({
  context: { users: [] },
  initial: "Login Page",
  states: {
    "Login Page": {
      on: {
        "Submit login": {
          target: "Checking for user",
        },
        "Go to signup": {
          target: "Signup Page",
        },
      },
    },
    "Dashboard Page": {
      on: {
        "Log out": {
          target: "Login Page",
        },
      },
    },
    "Signup Page": {
      on: {
        "Submit signup": {
          actions: "saveToSession",
          target: "Dashboard Page",
        },
        "Go to login": {
          target: "Login Page",
        },
      },
    },
    "Checking for user": {
      invoke: {
        src: "checkingForUser",
        onDone: [
          {
            cond: "User available",
            target: "Dashboard Page",
          },
          {
            target: "User not found",
          },
        ],
      },
    },
    "User not found": {
      after: {
        "2500": {
          target: "Login Page",
        },
      },
    },
  },
  id: "Black Parade Machine",
}, {
    services: {
        checkingForUser: async (context, event) => {
            // send({type: 'Submit login', user: {}})
            const { username, email, password } = event.user
            
            const usersFromSession = window.sessionStorage.getItem("users")
            
            if (usersFromSession !== null) {
                // change from JSOn string to JavaScript object
                const users = JSON.parse(usersFromSession) 
                context.users = users // save to context

                // find user iwth username
                if (username) {
                    let user = users.find(o => o.username === username)

                    if (user !== undefined) {
                        // return true if password matches
                        return user.password === password
                    }
                } else { // find user with email
                    let user = users.find(o => o.email === email)

                    if (user !== undefined) {
                        // return true if password matches
                        return user.password === password
                    }
                }
            }
            
            // if it reaches this point, it means that there are no user available
            return false
        },
    },
    actions: {
        saveToSession: (context, event) => {
            // send({type: 'Submit signup', user: {}})

            // get data from session array
            const usersFromSession = window.sessionStorage.getItem("users")
            console.log('usersFromSession', window.sessionStorage.getItem("users"))

            if (usersFromSession === null) {
                const listOfUsers = [event.user]
                window.sessionStorage.setItem("users", JSON.stringify(listOfUsers))
            
              } else {
                const users = JSON.parse(usersFromSession) 
                console.log('event.user', event.user)
                const newUsers = [ ...users, event.user]

                // save to session
                window.sessionStorage.setItem("users", JSON.stringify(newUsers))
            }
        }
    }, 
    guards: {
        "User available": (context, event) => {
            return event.data
        }
    }
})