import React, { Component, useState, useEffect } from 'react'

import Sidebar from '../components/sidebar'
import List from '../components/list'
import MobileSongList from '../components/mobileSongList'
import BottomNav from '../components/bottomNav'

import { data as songData } from '../data/mock_data'

const DesktopDashboard = ({ send }) => {
    const [searchedArray, setSearchedArray] = useState(songData.song_playlist.data)
    const [searchString, setSearchString] = useState("")
    const [musicNotFound, setMusicNotFound] = useState(false)
    
    useEffect(() => {
        const arr = songData.song_playlist.data

        if (searchString.length === 0) {
            setSearchedArray(arr)
            setMusicNotFound(false)
        } else {
            const filteredObjectsByName = arr.filter(item => item.song_name.toLowerCase().includes(searchString.toLowerCase()))

            if (filteredObjectsByName.length === 0) {
                setMusicNotFound(true)
            }  else {
                setSearchedArray(filteredObjectsByName)
            }
          }
    }, [searchString])

    return (
        <div className="min-h-screen max-w-full">
            {/* sidebar here */}
            <Sidebar send={send}/>
            {/* Header and search bar */}
            <div className="min-w-full min-h-screen pt-20 flex-1 bg-[#212024]">
                <div className="space-y-4 p-4 sm:px-8 sm:py-6 lg:p-4 xl:px-10 xl:pl-52">
                    <div className="flex items-center justify-between lg:pl-52">
                        <div>
                            <h2 className="lg:text-3xl sm:text-xl font-semibold text-white">My music list</h2>
                        </div>

                        <form
                            className="relative lg:-pr-20 mr-10">
                                <input 
                                    className="bg-[#2e2e32] appearance-none lg:w-full text-sm leading-6 text-[#E5E5E5] placeholder-[#E5E5E5] rounded-xl py-2 pl-10 shadow-sm" 
                                    type="text" 
                                    placeholder="Search"
                                    onChange={(event) => setSearchString(event.target.value)}
                                 />
                                <div className="absolute right-0 top-0 mt-3 mr-4">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="text-white bi bi-search" viewBox="0 0 16 16">
                                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                    </svg>
                                </div>
                        </form>
                    </div>
                    <p className="lg:text-lg sm:text-sm text-white lg:pl-52">My recent search list</p>
                    <div className="lg:pl-52 lg:ml-7 px-9 text-white w-full rounded py-4">
                        {/* list of cards here */}
                        <List searchData={searchedArray} status={musicNotFound}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

const MobileDashboard = ({ send }) => {  
    const [searchedArray, setSearchedArray] = useState(songData.song_playlist.data)
    const [searchString, setSearchString] = useState("")
    const [musicNotFound, setMusicNotFound] = useState(false)
    
    useEffect(() => {
        const arr = songData.song_playlist.data

        if (searchString.length === 0) {
            setSearchedArray(arr)
            setMusicNotFound(false)
        } else {
            const filteredObjectsByName = arr.filter(item => item.song_name.toLowerCase().includes(searchString.toLowerCase()))

            if (filteredObjectsByName.length === 0) {
                setMusicNotFound(true)
            }  else {
                setSearchedArray(filteredObjectsByName)
            }
          }
    }, [searchString])

    return (
        <div className="container p-8 text-white bg-[#212024] min-w-full min-h-screen">
            <div className="mx-auto max-w-lg">
                <div>
                    <form
                        className="relative lg:-pr-20 mr-10 mb-10">
                            <input 
                                className="bg-[#2e2e32] w-[110%] appearance-none lg:w-full text-sm leading-6 text-[#E5E5E5] placeholder-[#E5E5E5] rounded-xl py-2 pl-10 shadow-sm" 
                                type="text" 
                                placeholder="Search"
                                onChange={(event) => setSearchString(event.target.value)}
                                />
                            <div className="absolute left-[13px] top-0 mt-3 mr-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="text-white bi bi-search" viewBox="0 0 16 16">
                                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                </svg>
                            </div>
                        </form>
                </div>
                <div>
                    <h2 className="text-xl font-semibold">My music list</h2>
                    <p className="text-sm pt-2">My recent search list</p>
                </div>
                {/* Song list */}
                <MobileSongList searchData={searchedArray} status={musicNotFound} />

                {/* Bottom Navigation */}
                <BottomNav send={send}/>
            </div>
        </div>
    )
}

export default class DashboardPage extends Component {
    constructor(props) {
      super(props)
  
      this.state = { mobile: false }
    }
  
    componentDidMount(){
      window.addEventListener("resize", this.resize.bind(this))
      this.resize()
    }
  
    resize() {
      let currentMobile = (window.innerWidth < 821) // max width is the size of a tablet

      if (currentMobile !== this.state.mobile) {
          this.setState({ mobile: window.innerWidth < 821 })
      }
    }
  
    componentWillUnmount(){
      window.removeEventListener("resize", this.resize.bind(this))
    }
  
    render() {
      return (
        <div>
          {this.state.mobile ? <MobileDashboard send={this.props.send}/> : <DesktopDashboard send={this.props.send}/>}
        </div>
      )
    }
  }