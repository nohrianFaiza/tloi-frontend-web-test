import React, { Component } from 'react'

import MobileLayout from '../components/mobileLayout'
import Layout from '../components/layout'
import { LoginCard, MobileLoginCard } from '../components/loginCard'

const LoginMobile = ({ send }) => {
    return (
        <MobileLayout>
           <MobileLoginCard send={send}/>
        </MobileLayout>
    )
}

const LoginDesktop = ({ send }) => {
    return (
        <>
            <Layout />
            <LoginCard send={send} />
        </>
    )
}

export default class LoginPage extends Component {
  constructor(props) {
    super(props)

    this.state = { hideHero: false }
  }

  componentDidMount(){
    window.addEventListener("resize", this.resize.bind(this))
    this.resize()
  }

  resize() {
    let currentHideHero = (window.innerWidth < 821) // max width is the size of a tablet
    console.log(currentHideHero)
    
    if (currentHideHero !== this.state.hideHero) {
        this.setState({ hideHero: window.innerWidth < 821 })
    }
  }

  componentWillUnmount(){
    window.removeEventListener("resize", this.resize.bind(this))
  }

  render() {
    return (
      <div>
        {this.state.hideHero ? <LoginMobile send={this.props.send}/> : <LoginDesktop send={this.props.send}/>}
      </div>
    )
  }
}