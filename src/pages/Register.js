import React, { Component } from 'react'

import MobileLayout from '../components/mobileLayout'
import Layout from '../components/layout'
import { RegisterCard, MobileRegisterCard } from '../components/registerCard'

const RegisterMobile = ({ send }) => {
    return (
        <MobileLayout>
            <MobileRegisterCard send={send}/>
        </MobileLayout>
    )
}

const RegisterDesktop = ({ send }) => {
    return (
        <>
            <Layout />
            <RegisterCard  send={send} />
        </>
    )
}

export default class RegisterPage extends Component {
  constructor(props) {
    super(props)
    
    this.state = { hideHero: false }
  }

  componentDidMount(){
    window.addEventListener("resize", this.resize.bind(this))
    this.resize()
  }

  resize() {
    let currentHideHero = (window.innerWidth < 821) // max width is the size of a tablet
    console.log(currentHideHero)
    
    if (currentHideHero !== this.state.hideHero) {
        this.setState({ hideHero: window.innerWidth < 821 })
    }
  }

  componentWillUnmount(){
    window.removeEventListener("resize", this.resize.bind(this))
  }

  render() {
    return (
      <div>
        {this.state.hideHero ? <RegisterMobile send={this.props.send}/> : <RegisterDesktop send={this.props.send}/>}
    </div>
    )
  }
}